<?php
/**
 * Created by PhpStorm.
 * User: Saiya
 * Date: 8/31/2017
 * Time: 16:44
 */

namespace Louvre\BookingBundle\Controller;

use Doctrine\ORM\EntityManager;

use Louvre\BookingBundle\Entity\Tarif;
use Louvre\BookingBundle\Entity\Booking;
use Louvre\BookingBundle\Entity\Visitor;
use Louvre\BookingBundle\Form\BookingType;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Booking controller
 *
 * @Route("/")
 */
class BookingController extends Controller
{

    /**
     * Index
     *
     * @Route("/", name="louvre_home")
     * @return Response
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        $tarifs = $em->getRepository('LouvreBookingBundle:Tarif')->findAll();

        return $this->render('@LouvreBooking/Booking/index.html.twig', array(
            'tarifs' => $tarifs
        ));
    }

    /**
     * Creates a new booking
     *
     * @Route("/booking/", name="louvre_booking")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function bookingAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $booking = new Booking();
        $visitor = new Visitor();

        $visitor->setBooking($booking);

        $form = $this->createForm(BookingType::class, $booking);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
                foreach ($booking->getVisitors() as $visitor){
                    $visitor->setBooking($booking);
                }

                $booking->setTotalPrice($this->calculatePriceVisitors($booking));

                $em->persist($booking);
                $em->flush();

                return $this->redirectToRoute('louvre_booking_summary', array(
                    'id' => $booking->getId()
                ));
            }

        return $this->render('LouvreBookingBundle:Booking:booking.html.twig', array(
            'form' => $form->createView(),
            'booking' => $booking
        ));
    }

    /**
     * Summary
     *
     * @param Booking $booking
     * @return Response
     * @throws \Twig\Error\Error
     */
    public function summaryAction(Booking $booking) {
        $content = $this->get('templating')->render('LouvreBookingBundle:Booking:summary.html.twig', array(
            'booking' => $booking
        ));

        if ($booking->getPaid() == true) {
            return $this->render('LouvreBookingBundle:Booking:404.html.twig');
        }

        return new Response($content);
    }

    /**
     * Checkout
     *
     * @param Booking $booking
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function checkoutAction(Booking $booking) {
        if ($booking->getPaid() == true) {
            return $this->render('LouvreBookingBundle:Booking:404.html.twig');
        }

        \Stripe\Stripe::setApiKey("sk_test_cMbdS1znnopjRdQCElhBaMem");

        $token  = $_POST['stripeToken'];

        $customer = \Stripe\Customer::create(array(
            'email' => $booking->getEmail(),
            'source'  => $token
        ));

        $charge = \Stripe\Charge::create(array(
            'customer' => $customer->id,
            'amount'   => ($booking->getTotalPrice() * 100),
            'currency' => 'eur'
        ));

        return $this->redirectToRoute('louvre_booking_payment_processed', array(
            'id' => $booking->getId()
        ));
    }

    /**
     * Payment processed
     *
     * @param Booking $booking
     * @return Response
     * @throws \Twig\Error\Error
     */
    public function paymentProcessedAction(Booking $booking) {
        $em = $this->getDoctrine()->getManager();
        $content = $this->get('templating')->render('LouvreBookingBundle:Booking:payment-processed.html.twig', array(
            'booking' => $booking
        ));

        if ($booking->getPaid() == true) {
            return $this->render('LouvreBookingBundle:Booking:404.html.twig');
        }

        $booking->setBookingNumber(substr(str_shuffle('aBcEeFgHiJkLmNoPqRstUvWxYz0123456789'),0, 4));
        $booking->setPaid(true);

        $em->flush();

        $pdfFolder = $this->get('kernel')->getRootDir() . "/../web/pdf";


        $pdfTemplate = $this->renderView(
            'LouvreBookingBundle:Booking:booking-pdf.html.twig',
            array(
                'booking'  => $booking
            )
        );
        $attach = $pdfFolder . "/" . $booking->getId() . '-' . time() . '.pdf';
        $this->get('knp_snappy.pdf')->generateFromHtml($pdfTemplate, $attach);

        $message = (new \Swift_Message($this->get('translator')->trans('Reservation email - Louvre Museum')))
            ->setFrom('louvreapp@free.fr')
            ->setTo($booking->getEmail())
            ->setBody(
                $this->renderView(
                    'LouvreBookingBundle:Booking:booking-email.html.twig',
                    array(
                        'booking' => $booking
                    )
                ),
                'text/html'
            )
            ->attach(\Swift_Attachment::fromPath($attach));
        ;

        $this->get('mailer')->send($message);

        return new Response($content);
    }

    /**
     * About
     * @Route("/about/", name="louvre_about")
     * @return Response
     */
    public function aboutAction() {
        return $this->render('@LouvreBooking/Booking/about.html.twig');
    }

    /**
     * Contact
     *
     * @Route("/contact/", name="louvre_contact")
     * @return Response
     */
    public function contactAction() {
        return $this->render('@LouvreBooking/Booking/contact.html.twig');
    }

    /**
     * Get total price of Booking's visitor
     *
     * @param Booking $booking
     * @return int
     */
    private function calculatePriceVisitors(Booking $booking) {
        /** Get EntityManager for db transaction @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        /** Init total price @var int $totalPrice */
        $totalPrice = 0;

        /** Get each visitor of booking @var Visitor $visitor */
        foreach ($booking->getVisitors() as $visitor) {
            /** Get visitor's age @var int $age */
            $age = $visitor->getAge();

            /** Get TarifType of visitor by his age @var Tarif $tarifType */
            $tarifType = $em->getRepository('LouvreBookingBundle:Tarif')->findTarifByAge($age);

            /** If the booking is an half-day */
            if (1 == $booking->getBookingType()) {
                /** Does the visitor have reduction ? */
                if ($visitor->getReduction()) {
                    /** Add price to totalprice var (the price is multiplicated by 20, divided by 100 and divided by 2) */
                    $totalPrice += ($tarifType->getTarifPrice() - 10) / 2;
                } else {
                    /** Add price to totalprice var (the price is divided by 2) */
                    $totalPrice += $tarifType->getTarifPrice() / 2;
                }
            }
            /** Else if the booking is a complete day */
            else if (2 == $booking->getBookingType()) {
                /** Does the visitor have reduction ? */
                if ($visitor->getReduction()) {
                    /** Add price to totalprice var (the price is multiplicated by 20, divided by 100 and divided by 2) */
                    $totalPrice += $tarifType->getTarifPrice() - 10;
                } else {
                    /** Add price to totalprice var (the price is divided by 2) */
                    $totalPrice += $tarifType->getTarifPrice();
                }
            }
        }
        /** Return the totalPrice */
        return $totalPrice;
    }
}