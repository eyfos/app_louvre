<?php
/**
 * Created by PhpStorm.
 * User: Saiya
 * Date: 12/6/2017
 * Time: 12:59
 */

namespace Louvre\BookingBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Louvre\BookingBundle\Entity\Booking;
use Louvre\BookingBundle\Entity\Visitor;
use Louvre\BookingBundle\Entity\Tarif;


class LoadFixtures implements FixtureInterface, ContainerAwareInterface {

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function setContainer (ContainerInterface $container = NULL) {
        $this->container = $container;
    }

    private function loadTarif(ObjectManager $manager)
    {
        $options = array(
            'Senior' => 12,
            'Normal' => 16,
            'Young' => 8,
            'Free' => 0
        );

        foreach ($options as $key => $value) {
            $tarif = new Tarif();

            $tarif->setTarifType($key);
            $tarif->setTarifPrice($value);

            switch ($tarif->getTarifType()) {
                case 'Free':
                    $tarif->setTarifMinAge('0');
                    $tarif->setTarifMaxAge('3');
                    break;
                case 'Young':
                    $tarif->setTarifMinAge('4');
                    $tarif->setTarifMaxAge('12');
                    break;
                case 'Normal':
                    $tarif->setTarifMinAge('13');
                    $tarif->setTarifMaxAge('59');
                    break;
                case 'Senior':
                    $tarif->setTarifMinAge('60');
                    $tarif->setTarifMaxAge('150');
                    break;

            }

            $manager->persist($tarif);
        }

        $manager->flush();
    }

    private function loadBooking(ObjectManager $manager) {
        for ($i = 0; $i < 30; $i++) {
            $booking = new Booking();

            $booking->setBookingType(1);
            $booking->setEmail('gatienhrd@gmail.com');
            $booking->setVisitorNumber('3');
            $booking->setComment('Commentaire');

            for ($j = 0; $j < 1; $j++){
                $visitor = new Visitor();
                $visitor->setBooking($booking);
                $visitor->setName('Bob');
                $visitor->setFirstName('Boub');
                $visitor->setCountry('FR');
                $visitor->setBirthDate(new \DateTime('24-01-1996'));
                $booking->addVisitor($visitor);
                $manager->persist($visitor);
            }

            $booking->setTotalPrice($this->calculatePriceVisitors($booking, $manager));
            $booking->setPaid(true);

            $manager->persist($booking);
        }
    }

    private function calculatePriceVisitors(Booking $booking, ObjectManager $manager) {
        /** Init total price @var int $totalPrice */
        $totalPrice = 0;

        /** Get each visitor of booking @var Visitor $visitor */
        foreach ($booking->getVisitors() as $visitor) {
            /** Get visitor's age @var int $age */
            $age = $visitor->getAge();

            /** Get TarifType of visitor by his age @var Tarif $tarifType */
            $tarifType = $manager->getRepository('LouvreBookingBundle:Tarif')->findTarifByAge($age);

            /** If the booking is an half-day */
            if (1 == $booking->getBookingType()) {
                /** Does the visitor have reduction ? */
                if ($visitor->getReduction()) {
                    /** Add price to totalprice var (the price is multiplicated by 20, divided by 100 and divided by 2) */
                    $totalPrice += ($tarifType->getTarifPrice() - 10) / 2;
                } else {
                    /** Add price to totalprice var (the price is divided by 2) */
                    $totalPrice += $tarifType->getTarifPrice() / 2;
                }
            }
            /** Else if the booking is a complete day */
            else if (2 == $booking->getBookingType()) {
                /** Does the visitor have reduction ? */
                if ($visitor->getReduction()) {
                    /** Add price to totalprice var (the price is multiplicated by 20, divided by 100 and divided by 2) */
                    $totalPrice += $tarifType->getTarifPrice() - 10;
                } else {
                    /** Add price to totalprice var (the price is divided by 2) */
                    $totalPrice += $tarifType->getTarifPrice();
                }
            }
        }
        /** Return the totalPrice */
        return $totalPrice;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadTarif($manager);
        $this->loadBooking($manager);

        $manager->flush();
    }
}