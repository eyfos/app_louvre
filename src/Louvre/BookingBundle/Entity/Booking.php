<?php

namespace Louvre\BookingBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Louvre\BookingBundle\Validator\Constraints as Louvre;

/**
 * @ORM\Entity(repositoryClass="Louvre\BookingBundle\Repository\BookingRepository")
 * @Louvre\LessThan1000Visitors()
 * @Louvre\BlockDayBookingAfter14H()
 */
class Booking
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datePurchase", type="datetime")
     * @Assert\NotBlank()
     * @Assert\DateTime(
     *     format="d-m-Y",
     *     message="Incorrect date format. Expected format : '{{ format }}'."
     * )
     */
    private $datePurchase;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateBooking", type="datetime")
     * @Assert\NotBlank()
     * @Assert\DateTime(
     *     format="d-m-Y",
     *     message="Incorrect date format. Expected format : '{{ format }}'."
     * )
     * @Assert\GreaterThanOrEqual("today")
     */
    private $dateBooking;

    /**
     * @var string
     *
     * @ORM\Column(name="bookingType", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="integer",
     *     message="The value '{{ value }}' is not a valid value of type '{{ type }}'."
     * )
     */
    private $bookingType;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid address.",
     *     checkMX = true
     * )
     */
    private $email;

    /**
     * @var int
     *
     * @ORM\Column(name="visitorNumber", type="integer")
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="integer",
     *     message="The value '{{ value }}' is not a valid value of type '{{ type }}'."
     * )
     */
    private $visitorNumber;

    /**
     * @var \Louvre\BookingBundle\Entity\Visitor
     *
     * @ORM\OneToMany(targetEntity="Louvre\BookingBundle\Entity\Visitor", mappedBy="booking", cascade={"persist"})
     * @Assert\Valid()
     */
    private $visitors;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="totalPrice", type="integer")
     */
    private $totalPrice;

    /**
     * @var bool
     *
     * @ORM\Column(name="paid", type="boolean")
     */
    private $paid;

    /**
     * @var string
     *
     * @ORM\Column(name="bookingNumber", type="string", length=255, nullable=true)
     */
    private $bookingNumber;

    public function __construct()
    {
        $this->datePurchase = new \Datetime();
        $this->dateBooking = new \Datetime();
        $this->paid = false;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datePurchase
     *
     * @param \DateTime $datePurchase
     *
     * @return Booking
     */
    public function setDatePurchase($datePurchase)
    {
        $this->datePurchase = $datePurchase;

        return $this;
    }

    /**
     * Get datePurchase
     *
     * @return \DateTime
     */
    public function getDatePurchase()
    {
        return $this->datePurchase;
    }

    /**
     * Set dateBooking
     *
     * @param \DateTime $dateBooking
     *
     * @return Booking
     */
    public function setDateBooking($dateBooking)
    {
        $this->dateBooking = $dateBooking;

        return $this;
    }

    /**
     * Get dateBooking
     *
     * @return \DateTime
     */
    public function getDateBooking()
    {
        return $this->dateBooking;
    }

    /**
     * Set bookingType
     *
     * @param string $bookingType
     *
     * @return Booking
     */
    public function setBookingType($bookingType)
    {
        $this->bookingType = $bookingType;

        return $this;
    }

    /**
     * Get bookingType
     *
     * @return string
     */
    public function getBookingType()
    {
        return $this->bookingType;
    }

    /**
     * Set visitorNumber
     *
     * @param integer $visitorNumber
     *
     * @return Booking
     */
    public function setVisitorNumber($visitorNumber)
    {
        $this->visitorNumber = $visitorNumber;

        return $this;
    }

    /**
     * Get visitorNumber
     *
     * @return integer
     */
    public function getVisitorNumber()
    {
        return $this->visitorNumber;
    }

    /**
     * Set totalPrice
     *
     * @param integer $totalPrice
     *
     * @return integer
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**
     * Get totalPrice
     *
     * @return integer
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * Add visitor
     *
     * @param \Louvre\BookingBundle\Entity\Visitor $visitor
     *
     * @return Booking
     */
    public function addVisitor(\Louvre\BookingBundle\Entity\Visitor $visitor)
    {
        $this->visitors[] = $visitor;

        return $this;
    }

    /**
     * Remove visitor
     *
     * @param \Louvre\BookingBundle\Entity\Visitor $visitor
     */
    public function removeVisitor(\Louvre\BookingBundle\Entity\Visitor $visitor)
    {
        $this->visitors->removeElement($visitor);
    }

    /**
     * Get visitors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVisitors()
    {
        return $this->visitors;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Booking
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Booking
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set paid
     *
     * @param boolean $paid
     *
     * @return Booking
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;

        return $this;
    }

    /**
     * Get paid
     *
     * @return boolean
     */
    public function getPaid()
    {
        return $this->paid;
    }

    /**
     * Set bookingNumber
     *
     * @param string $bookingNumber
     *
     * @return Booking
     */
    public function setBookingNumber($bookingNumber)
    {
        $this->bookingNumber = $bookingNumber;

        return $this;
    }

    /**
     * Get bookingNumber
     *
     * @return string
     */
    public function getBookingNumber()
    {
        return $this->bookingNumber;
    }
}
