<?php

namespace Louvre\BookingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tarif
 *
 * @ORM\Table(name="tarif")
 * @ORM\Entity(repositoryClass="Louvre\BookingBundle\Repository\TarifRepository")
 */
class Tarif
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tarifType", type="string", length=255)
     */
    private $tarifType;

    /**
     * @var int
     *
     * @ORM\Column(name="tarifPrice", type="integer")
     */
    private $tarifPrice;

    /**
     * @var int
     *
     * @ORM\Column(name="tarifMinAge", type="integer")
     */
    private $tarifMinAge;

    /**
     * @var int
     *
     * @ORM\Column(name="tarifMaxAge", type="integer")
     */
    private $tarifMaxAge;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tarifType
     *
     * @param string $tarifType
     *
     * @return Tarif
     */
    public function setTarifType($tarifType)
    {
        $this->tarifType = $tarifType;

        return $this;
    }

    /**
     * Get tarifType
     *
     * @return string
     */
    public function getVisitorType()
    {
        return $this->tarifType;
    }

    /**
     * Set tarifPrice
     *
     * @param integer $tarifPrice
     *
     * @return Tarif
     */
    public function setTarifPrice($tarifPrice)
    {
        $this->tarifPrice = $tarifPrice;

        return $this;
    }

    /**
     * Get tarifPrice
     *
     * @return int
     */
    public function getTarifPrice()
    {
        return $this->tarifPrice;
    }

    /**
     * Get tarifType
     *
     * @return string
     */
    public function getTarifType()
    {
        return $this->tarifType;
    }


    /**
     * Set tarifMinAge
     *
     * @param integer $tarifMinAge
     *
     * @return Tarif
     */
    public function setTarifMinAge($tarifMinAge)
    {
        $this->tarifMinAge = $tarifMinAge;

        return $this;
    }

    /**
     * Get tarifMinAge
     *
     * @return integer
     */
    public function getTarifMinAge()
    {
        return $this->tarifMinAge;
    }

    /**
     * Set tarifMaxAge
     *
     * @param integer $tarifMaxAge
     *
     * @return Tarif
     */
    public function setTarifMaxAge($tarifMaxAge)
    {
        $this->tarifMaxAge = $tarifMaxAge;

        return $this;
    }

    /**
     * Get tarifMaxAge
     *
     * @return integer
     */
    public function getTarifMaxAge()
    {
        return $this->tarifMaxAge;
    }
}
