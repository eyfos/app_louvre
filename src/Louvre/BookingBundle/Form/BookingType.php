<?php

namespace Louvre\BookingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

use Symfony\Component\Validator\Constraints\Valid;

class BookingType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateBooking',    DateType::class, array(
                'label' => 'Booking date',
                'widget' => 'single_text',
                'html5' => false,
                'error_bubbling' => true,
                'format' => 'dd-MM-yyyy',
                'attr' => [
                    'class' => 'js-datepicker form-control',
                    'data-date-format' => 'dd-mm-yyyy'
                ]
            ))
            ->add('bookingType',   ChoiceType::class, array(
                'label' => 'Booking type',
                'attr' => [
                    'class' => 'form-control',
                ],
                'error_bubbling' => true,
                'choices' => array(
                    'Half-day' => 1,
                    'Day' => 2,
                )
            ))
            ->add('visitorNumber',     ChoiceType::class, array(
                'label' => 'Number of visitors',
                'attr' => [
                    'class' => 'form-control',
                ],
                'choices' => array(
                    '1 visitor' => 1,
                    '2 visitors' => 2,
                    '3 visitors' => 3,
                    '4 visitors' => 4,
                    '5 visitors' => 5,
                    '6 visitors' => 6,
                )
            ))
            ->add('email', EmailType::class, array(
                'label' => 'Email',
                'error_bubbling' => true,
                'attr' => [
                    'class' => 'form-control',
                ],
            ))
            ->add('visitors', CollectionType::class, array(
                'label' => ' ',
                'entry_type' => VisitorType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'constraints' => array(new Valid())
                )
            )
            ->add('comment', TextareaType::class, array(
                'label' => 'Comment (optional)',
                'attr' => ['class' => 'form-control'],
                'required'   => false,
            ))
            ->add('save',      SubmitType::class, array(
                'label' => 'Book ticket',
                'attr' => ['class' => 'btn btn-lg'],
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Louvre\BookingBundle\Entity\Booking'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'louvre_bookingbundle_booking';
    }
}
