<?php

namespace Louvre\BookingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class VisitorType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Name',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Name',
                ],
                'error_bubbling' => true
            ))
            ->add('firstName', TextType::class, array(
                'label' => 'Firstname',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Firstname'
                ],
                'error_bubbling' => true
            ))
            ->add('country', CountryType::class, array(
                'label' => 'Country',
                'attr' => [
                    'class' => 'form-control',
                ],
                'preferred_choices' => array('FR')
            ))
            ->add('birthDate', BirthdayType::class, array(
                'label' => 'Birthdate',
                'error_bubbling' => true,
                'attr' => [
                    'class' => 'birthDate-label',
                ]
            ))
            ->add('reduction',  CheckboxType::class, array(
                'label' => 'Reduction',
                'required' => false,
                'attr' => [
                    'class' => 'reduction',
                ],
            ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Louvre\BookingBundle\Entity\Visitor'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'louvre_bookingbundle_visitor';
    }
}
