<?php

namespace Louvre\BookingBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class BlockDayBookingAfter14H extends Constraint
{
    public $message = 'Impossible de réserver un billet de type \'Journée\' après 14h pour aujourd\'hui.';

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}