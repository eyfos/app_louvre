<?php

namespace Louvre\BookingBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class BlockDayBookingAfter14HValidator extends ConstraintValidator
{
    public function validate($protocol, Constraint $constraint)
    {
        $dateBooking = $protocol->getDateBooking();
        $today = new \DateTime();
        $twoPm = \DateTime::createFromFormat("H:i:s", "14:00:00");

        if($dateBooking->format("d/m/Y") == date("d/m/Y")) {
            if($today >= $twoPm && $protocol->getBookingType() == 2) {
                $this->context->addViolation($constraint->message);
            }
        }
    }
}