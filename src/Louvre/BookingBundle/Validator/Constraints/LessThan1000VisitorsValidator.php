<?php

namespace Louvre\BookingBundle\Validator\Constraints;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Doctrine\ORM\EntityManagerInterface;

class LessThan1000VisitorsValidator extends ConstraintValidator
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function validate($protocol, Constraint $constraint)
    {
        $isLessThan1000Visitors = $this->em
            ->getRepository('LouvreBookingBundle:Visitor')
            ->countVisitorsByDate($protocol->getDateBooking())
        ;

        if ($isLessThan1000Visitors >= 1000) {
            $this->context->addViolation($constraint->message);
        }
    }
}