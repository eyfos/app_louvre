<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class BookingControllerTest extends WebTestCase {

    public function testHomepageIsUp()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertSame(200, $client->getResponse()->getStatusCode());
    }

    public function testBookingIsUp()
    {
        $client = static::createClient();
        $client->request('GET', '/booking');

        $this->assertSame(200, $client->getResponse()->getStatusCode());
    }

    public function testCorrectBookingOneVisitor()
    {
        $dateObject = new \DateTime();
        $dateBooking = $dateObject->format('d-m-Y');

        // Create client
        $client = static::createClient();

        $crawler = $client->request('GET', '/booking');

        // Select button and form
        $submitBookingButton = $crawler->selectButton('Réserver');
        $form = $submitBookingButton->form();

        // Form values
        $bookingValues = array(
            'louvre_bookingbundle_booking' => array(
                '_token' => $form['louvre_bookingbundle_booking[_token]']->getValue(),
                'dateBooking'   => $dateBooking,
                'bookingType'   => 1,
                'visitorNumber'   => 1,
                'email'   => 'gatienhrd@gmail.com',
                'visitors' => array(
                    1 => array(
                        'name' => 'John',
                        'firstName'   => 'Doe',
                        'country'   => 'France',
                        'birthDate'   => array(
                            'month' => 1,
                            'day' => '24',
                            'year' => '1996'
                        ),
                    ),
                ),
            ),
        );

        // Perform POST request
        $client->request($form->getMethod(), $form->getUri(), $bookingValues);


        // Redirected to summary route
        $this->assertSame(302, $client->getResponse()->getStatusCode());
        // Find element in summary view
        $this->assertSame(1, $crawler->filter('div.go-booking')->count());
    }

    public function testCorrectBookingSeveralVisitors()
    {
        $dateObject = new \DateTime();
        $dateBooking = $dateObject->format('d-m-Y');

        // Create client
        $client = static::createClient();

        $crawler = $client->request('GET', '/booking');

        // Select button and form
        $submitBookingButton = $crawler->selectButton('Réserver');
        $form = $submitBookingButton->form();

        // Form values
        $bookingValues = array(
            'louvre_bookingbundle_booking' => array(
                '_token' => $form['louvre_bookingbundle_booking[_token]']->getValue(),
                'dateBooking'   => $dateBooking,
                'bookingType'   => 1,
                'visitorNumber'   => 3,
                'email'   => 'gatienhrd@gmail.com',
                'visitors' => array(
                    1 => array(
                        'name' => 'Dupont',
                        'firstName'   => 'Michel',
                        'country'   => 'France',
                        'birthDate'   => array(
                            'month' => 1,
                            'day' => '24',
                            'year' => '1996'
                        ),
                    ),
                    2 => array(
                        'name' => 'Dubois',
                        'firstName'   => 'Georges',
                        'country'   => 'France',
                        'birthDate'   => array(
                            'month' => 1,
                            'day' => '24',
                            'year' => '1996'
                        ),
                    ),
                    3 => array(
                        'name' => 'Dumoulin',
                        'firstName'   => 'Greg',
                        'country'   => 'France',
                        'birthDate'   => array(
                            'month' => 1,
                            'day' => '24',
                            'year' => '1996'
                        ),
                    ),
                ),
            ),
        );

        // Perform POST request
        $client->request($form->getMethod(), $form->getUri(), $bookingValues);


        // Check redirection to summary route
        $this->assertSame(302, $client->getResponse()->getStatusCode());
        // Find element in summary view
        $this->assertSame(1, $crawler->filter('div.go-booking')->count());
    }

    public function testIncorrectBookingEmptyFields()
    {
        $dateObject = new \DateTime();
        $dateBooking = $dateObject->format('d-m-Y');

        // Create client
        $client = static::createClient();

        $crawler = $client->request('GET', '/booking');

        // Select button and form
        $submitBookingButton = $crawler->selectButton('Réserver');
        $form = $submitBookingButton->form();

        // Form values
        $bookingValues = array(
            'louvre_bookingbundle_booking' => array(
                '_token' => $form['louvre_bookingbundle_booking[_token]']->getValue(),
                'dateBooking'   => $dateBooking,
                'bookingType'   => 1,
                'visitorNumber'   => 1,
                'email'   => 'gatienhrd@gmail.com',
                'visitors' => array(
                    0 => array(
                        'name' => '', // Empty field
                        'firstName'   => '', // Empty field
                        'country'   => 'France',
                        'birthDate'   => array(
                            'month' => 1,
                            'day' => '24',
                            'year' => '1996'
                        ),
                    ),
                ),
            ),
        );

        // Perform POST request
        $client->request($form->getMethod(), $form->getUri(), $bookingValues);

        // Check that POST request failed, no redirection, status code = 200
        $this->assertSame(200, $client->getResponse()->getStatusCode());
    }

    public function testIncorrectBookingPreviousDate()
    {
        // Create client
        $client = static::createClient();

        $crawler = $client->request('GET', '/booking');

        // Select button and form
        $submitBookingButton = $crawler->selectButton('Réserver');
        $form = $submitBookingButton->form();

        // Form values
        $bookingValues = array(
            'louvre_bookingbundle_booking' => array(
                '_token' => $form['louvre_bookingbundle_booking[_token]']->getValue(),
                'dateBooking'   => '01-10-2016',
                'bookingType'   => 1,
                'visitorNumber'   => 1,
                'email'   => 'gatienhrd@gmail.com',
                'visitors' => array(
                    0 => array(
                        'name' => 'Dumoulin',
                        'firstName'   => 'Bob',
                        'country'   => 'France',
                        'birthDate'   => array(
                            'month' => 1,
                            'day' => '24',
                            'year' => '1996'
                        ),
                    ),
                ),
            ),
        );

        // Perform POST request
        $client->request($form->getMethod(), $form->getUri(), $bookingValues);

        // Check that POST request failed, no redirection, status code = 200
        $this->assertSame(200, $client->getResponse()->getStatusCode());
    }

    public function testIncorrectBookingUnknownEmail()
    {
        $dateObject = new \DateTime();
        $dateBooking = $dateObject->format('d-m-Y');

        // Create client
        $client = static::createClient();

        $crawler = $client->request('GET', '/booking');

        // Select button and form
        $submitBookingButton = $crawler->selectButton('Réserver');
        $form = $submitBookingButton->form();

        // Form values
        $bookingValues = array(
            'louvre_bookingbundle_booking' => array(
                '_token' => $form['louvre_bookingbundle_booking[_token]']->getValue(),
                'dateBooking'   => $dateBooking,
                'bookingType'   => 1,
                'visitorNumber'   => 1,
                'email'   => 'gatienhrd@hfhfjhghg.com',
                'visitors' => array(
                    0 => array(
                        'name' => 'Dupont',
                        'firstName'   => 'Michel',
                        'country'   => 'France',
                        'birthDate'   => array(
                            'month' => 1,
                            'day' => '24',
                            'year' => '1996'
                        ),
                    ),
                ),
            ),
        );

        // Perform POST request
        $client->request($form->getMethod(), $form->getUri(), $bookingValues);

        // Check that POST request failed, no redirection, status code = 200
        $this->assertSame(200, $client->getResponse()->getStatusCode());
    }

    public function testBookingEmailSent() {

        $client = static::createClient();

        // Enable the profiler for the next request (it does nothing if the profiler is not available)
        $client->enableProfiler();

        $crawler = $client->request('POST', '/booking/1/payment-processed');

        $mailCollector = $client->getProfile()->getCollector('swiftmailer');

        // Check that an email was sent
        $this->assertEquals(1, $mailCollector->getMessageCount());

        $collectedMessages = $mailCollector->getMessages();
        $message = $collectedMessages[0];

        // Asserting email data
        $this->assertInstanceOf('Swift_Message', $message);
        $this->assertEquals('Hello Email', $message->getSubject());
        $this->assertEquals('louvreapp@free.fr', key($message->getFrom()));
        $this->assertEquals('gatienhrd@gmail.com', key($message->getTo()));
        $this->assertEquals(
            'Merci d\'avoir reservé, voilà vos billets:',
            $message->getBody()
        );
    }






    /*public function testBooking($formData,$status)
    {
        $client = static::createClient();

        $parent = $client->getContainer()->get("doctrine.orm.entity_manager")->getRepository(Booking::class)->findOneByParent(null);

        $booking = $client->getContainer()->get("doctrine.orm.entity_manager")->getRepository(Booking::class);


        $client->request('GET', $client->getContainer()->get("router")->generate("louvre_booking"));
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());


        $client->request('POST', $client->getContainer()->get("router")->generate("louvre_booking"), $formData);
        $this->assertEquals($status, $client->getResponse()->getStatusCode());

        echo $client->getResponse()->getContent();

    }

    public function dataTest()
    {
        return [
            [
                "formData" => [
                    "booking" => [
                        "dateBooking" => "01-0-2017",
                        "bookingType" => 1,
                        "visitorNumber" => 1,
                        "email" => "gatienhrd@gmail.com",
                        "visitors" => [
                            [
                                "name" => "Dupont",
                                "firstName" => "Michel",
                                "country" => "France",
                                "birthDate" => "24-01-1996"
                            ],
                        ],
                    ],
                ],
                "status" => Response::HTTP_OK
            ],
        ];
    }*/

}